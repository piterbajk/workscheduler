﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WorkScheduler.Models
{
    public enum Status { NotAssigned, Waiting, InProgress, Cancelled, Done }
    public enum Priority { Low, Normal, High }
    public class Service
    {
        public int ServiceId { get; set; }
        public int? OrderId { get; set; }
        public virtual Order Order { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public string AssignedEmployeeId { get; set; }
        public virtual ApplicationUser AssignedEmployee { get; set; }
        public Priority Priority { get; set; }
        public Status Status { get; set; }

        public TimeSpan? Duration { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? Deadline { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? StartTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? EndTime { get; set; }

        void AssignEmployee(ApplicationUser employee)
        {
            AssignedEmployee = employee;
            Status = Status.Waiting;
        }
        void CalculateEndTime()
        {
            EndTime = StartTime + Duration;
        }
    }
}