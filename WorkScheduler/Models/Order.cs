﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WorkScheduler.Models
{
    public enum PaymentMethod { Cash, CreditCard, BankTransfer, Instalments }
    public class Order
    {
        public int OrderId { get; set; }
        public int? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public DateTime PlacementDate { get; set; }
        public bool IsCompleted { get; set; }
        public virtual ICollection<Service> Services { get; set; } 
    }
}