namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MaybeItWillHelp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "StartTime", c => c.DateTime());
            DropColumn("dbo.Services", "CreateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "CreateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Services", "StartTime");
        }
    }
}
