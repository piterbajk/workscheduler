namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedTimeTable3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "CreateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Services", "StartTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "StartTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Services", "CreateTime");
        }
    }
}
