namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeAssigned : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Services", name: "Employee_Id", newName: "EmployeeAssigned_Id");
            RenameIndex(table: "dbo.Services", name: "IX_Employee_Id", newName: "IX_EmployeeAssigned_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Services", name: "IX_EmployeeAssigned_Id", newName: "IX_Employee_Id");
            RenameColumn(table: "dbo.Services", name: "EmployeeAssigned_Id", newName: "Employee_Id");
        }
    }
}
