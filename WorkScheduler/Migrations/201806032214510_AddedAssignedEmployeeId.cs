namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAssignedEmployeeId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Services", name: "AssignedEmployee_Id", newName: "AssignedEmployeeId");
            RenameIndex(table: "dbo.Services", name: "IX_AssignedEmployee_Id", newName: "IX_AssignedEmployeeId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Services", name: "IX_AssignedEmployeeId", newName: "IX_AssignedEmployee_Id");
            RenameColumn(table: "dbo.Services", name: "AssignedEmployeeId", newName: "AssignedEmployee_Id");
        }
    }
}
