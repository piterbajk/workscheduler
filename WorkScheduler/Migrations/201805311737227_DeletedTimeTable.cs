namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedTimeTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TimeTables", "Service_ServiceId", "dbo.Services");
            DropIndex("dbo.TimeTables", new[] { "Service_ServiceId" });
            AddColumn("dbo.Services", "ExecutionOrder", c => c.Int(nullable: false));
            AddColumn("dbo.Services", "Duration", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.Services", "CreateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Services", "EndTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Services", "Deadline", c => c.DateTime(nullable: false));
            AddColumn("dbo.Services", "Priority", c => c.Int(nullable: false));
            AddColumn("dbo.Services", "Status", c => c.Int(nullable: false));
            DropTable("dbo.TimeTables");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TimeTables",
                c => new
                    {
                        TimeTableId = c.Int(nullable: false, identity: true),
                        ExecutionOrder = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        Deadline = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Service_ServiceId = c.Int(),
                    })
                .PrimaryKey(t => t.TimeTableId);
            
            DropColumn("dbo.Services", "Status");
            DropColumn("dbo.Services", "Priority");
            DropColumn("dbo.Services", "Deadline");
            DropColumn("dbo.Services", "EndTime");
            DropColumn("dbo.Services", "CreateTime");
            DropColumn("dbo.Services", "Duration");
            DropColumn("dbo.Services", "ExecutionOrder");
            CreateIndex("dbo.TimeTables", "Service_ServiceId");
            AddForeignKey("dbo.TimeTables", "Service_ServiceId", "dbo.Services", "ServiceId");
        }
    }
}
