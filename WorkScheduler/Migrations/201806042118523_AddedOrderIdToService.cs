namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrderIdToService : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Services", name: "Order_OrderId", newName: "OrderId");
            RenameIndex(table: "dbo.Services", name: "IX_Order_OrderId", newName: "IX_OrderId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Services", name: "IX_OrderId", newName: "IX_Order_OrderId");
            RenameColumn(table: "dbo.Services", name: "OrderId", newName: "Order_OrderId");
        }
    }
}
