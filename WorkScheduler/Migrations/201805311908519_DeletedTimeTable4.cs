namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedTimeTable4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Services", "Duration", c => c.Time(precision: 7));
            AlterColumn("dbo.Services", "Deadline", c => c.DateTime());
            AlterColumn("dbo.Services", "EndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Services", "EndTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Services", "Deadline", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Services", "Duration", c => c.Time(nullable: false, precision: 7));
        }
    }
}
