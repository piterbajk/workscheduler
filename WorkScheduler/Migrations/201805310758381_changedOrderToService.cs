namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedOrderToService : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TimeTables", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.TimeTables", new[] { "Order_OrderId" });
            AddColumn("dbo.TimeTables", "Service_ServiceId", c => c.Int());
            CreateIndex("dbo.TimeTables", "Service_ServiceId");
            AddForeignKey("dbo.TimeTables", "Service_ServiceId", "dbo.Services", "ServiceId");
            DropColumn("dbo.TimeTables", "Order_OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TimeTables", "Order_OrderId", c => c.Int());
            DropForeignKey("dbo.TimeTables", "Service_ServiceId", "dbo.Services");
            DropIndex("dbo.TimeTables", new[] { "Service_ServiceId" });
            DropColumn("dbo.TimeTables", "Service_ServiceId");
            CreateIndex("dbo.TimeTables", "Order_OrderId");
            AddForeignKey("dbo.TimeTables", "Order_OrderId", "dbo.Orders", "OrderId");
        }
    }
}
