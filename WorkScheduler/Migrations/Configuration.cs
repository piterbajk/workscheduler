namespace WorkScheduler.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WorkScheduler.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WorkScheduler.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WorkScheduler.Models.ApplicationDbContext";
        }

        protected override void Seed(WorkScheduler.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            AddSystemAdministrator(context);
        }

        void AddSystemAdministrator(WorkScheduler.Models.ApplicationDbContext context)
        {
            var admin = new ApplicationUser { UserName = "admin@email.com", Email = "admin@email.com" };
            var adminPassword = "admin";
            var userManager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));

            userManager.Create(admin, adminPassword);

        }
    }
}
