namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedExecOrderAndType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Services", "ExecutionOrder");
            DropColumn("dbo.Services", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Services", "ExecutionOrder", c => c.Int(nullable: false));
        }
    }
}
