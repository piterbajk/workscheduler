namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedTimeTable2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Services", name: "EmployeeAssigned_Id", newName: "AssignedEmployee_Id");
            RenameIndex(table: "dbo.Services", name: "IX_EmployeeAssigned_Id", newName: "IX_AssignedEmployee_Id");
            AddColumn("dbo.Services", "StartTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Services", "CreateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "CreateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Services", "StartTime");
            RenameIndex(table: "dbo.Services", name: "IX_AssignedEmployee_Id", newName: "IX_EmployeeAssigned_Id");
            RenameColumn(table: "dbo.Services", name: "AssignedEmployee_Id", newName: "EmployeeAssigned_Id");
        }
    }
}
