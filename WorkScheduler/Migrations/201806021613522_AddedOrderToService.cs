namespace WorkScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrderToService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "Order_OrderId", c => c.Int());
            CreateIndex("dbo.Services", "Order_OrderId");
            AddForeignKey("dbo.Services", "Order_OrderId", "dbo.Orders", "OrderId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Services", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.Services", new[] { "Order_OrderId" });
            DropColumn("dbo.Services", "Order_OrderId");
        }
    }
}
