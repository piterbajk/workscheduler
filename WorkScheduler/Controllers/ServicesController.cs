﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkScheduler.Models;
using Microsoft.AspNet.Identity;

namespace WorkScheduler.Controllers
{
    public class ServicesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string FindEmployeeIdWithLeastWorkAssigned()
        {
            var __debug_0 = db.Services
                .Where(x => x.AssignedEmployee.Position == Position.ProductionWorker)
                .ToList();

            var __debug_1 = db.Services
                .Where(x => x.AssignedEmployee.Position == Position.CustomerService)
                .ToList();

            var userId = db.Services
                .Where(x => x.AssignedEmployee.Position == Position.ProductionWorker)
                .GroupBy(x => x.AssignedEmployeeId)
                .Select(x => new
                {
                    AssignedEmployeeId = x.Key,
                    EndTime = x.Max(s => s.EndTime)
                })
                .OrderBy(x => x.EndTime)
                .Select(x => x.AssignedEmployeeId)
                .FirstOrDefault();

            var userWithoutAnyService = db.Users
                .Where(x => x.Position == Position.ProductionWorker)
                .Where(x => x.Services.Count() == 0)
                .FirstOrDefault();

            if (userWithoutAnyService != null)
            {
                return userWithoutAnyService.Id;
            }
            else
            {
                return userId;
            }
        }

        private DateTime? getEndTimeOfLastService(string userId)
        {
            var endTimeOfLastService = db.Services
                .Where(x => x.AssignedEmployee.Id == userId)
                .OrderByDescending(x => x.EndTime)
                .Select(x => x.EndTime)
                .FirstOrDefault();

            return endTimeOfLastService;
        }

        // GET: Services
        public async Task<ActionResult> Index()
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault
                (x => x.Id == currentUserId);
            var services = await db.Services.ToListAsync();
            if (currentUser.Position == Position.ProductionWorker)
            {
                return View(services.Where(x => x.AssignedEmployee == currentUser).OrderByDescending(x => x.StartTime));
            }
            else
            {
                return View(services.OrderByDescending(x => x.StartTime));
            }
        }

        // GET: Services/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Services/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CreateForOrder(int orderId)
        {
            Service service = new Service { OrderId = orderId };
            //Order order = new Order { OrderId = orderId };
            return View("Create", service);
        }

        // POST: Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ServiceId,Name,Description,Cost,Priority,Status,Duration,Deadline,StartTime,EndTime,OrderId")] Service service)
        {
            if (ModelState.IsValid)
            {
                /*
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault
                    (x => x.Id == currentUserId);
                service.AssignedEmployee = currentUser;
                */
                service.Status = Status.NotAssigned;

                // Find and assign employee with least work
                string employeeIdWithLeastWork = FindEmployeeIdWithLeastWorkAssigned();
                service.AssignedEmployee = db.Users.FirstOrDefault
                    (x => x.Id == employeeIdWithLeastWork);
                service.AssignedEmployeeId = service.AssignedEmployee.Id;
                service.Status = Status.Waiting;

                // Calculate start and end time 
                var EndTimeOfLastTask = getEndTimeOfLastService(service.AssignedEmployeeId);
                if (EndTimeOfLastTask == null || EndTimeOfLastTask < DateTime.Now)
                {
                    EndTimeOfLastTask = DateTime.Now;
                }       
                service.StartTime = EndTimeOfLastTask;
                service.EndTime = service.StartTime + service.Duration;

                db.Services.Add(service);
                await db.SaveChangesAsync();

                if (service.OrderId.HasValue)
                {
                    return RedirectToAction("Edit", "Orders", new { id = service.OrderId });
                }

                return RedirectToAction("Index");
            }

            return View(service);
        }

        // GET: Services/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ServiceId,Name,Description,Cost,Priority,Status,Duration,Deadline,StartTime,EndTime,OrderId")] Service service)
        {
            if (ModelState.IsValid)
            {
                //var db.Services.Where(s => s.ServiceId == service.ServiceId).FirstOrDefault();
                db.Entry(service).State = EntityState.Modified;
                await db.SaveChangesAsync();
                if (service.OrderId.HasValue)
                {
                    return RedirectToAction("Edit", "Orders", new { id = service.OrderId });
                }
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: Services/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            
            return View(service);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Service service = await db.Services.FindAsync(id);
            db.Services.Remove(service);
            await db.SaveChangesAsync();
            if (service.OrderId.HasValue)
            {
                return RedirectToAction("Edit", "Orders", new { id = service.OrderId });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
