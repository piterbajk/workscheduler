﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkScheduler.Models;

namespace WorkScheduler.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUser getCurrentUser()
        {
            string currentUserId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault
                (x => x.Id == currentUserId);
            return currentUser;
        }

        public ActionResult Index()
        {
            if(User.Identity.IsAuthenticated)
            {
                var currentUser = getCurrentUser(); 
                if (currentUser.Position == Position.ProductionWorker)
                {
                    return View("ProductionWorkerView", "_LayoutProductionWorker");
                }
                else if(currentUser.Position == Position.CustomerService)
                {
                    return View("CustomerServiceView", "_LayoutCustomerService");
                }
            }
            return View();
        }

        public ActionResult ProductionWorkerView()
        {
            var currentUser = getCurrentUser();
            ViewBag.Message = "Welcome Production Worker :"+currentUser.FirstName+" "+currentUser.LastName+"!";

            return View();
        }

        public ActionResult CustomerServiceView()
        {
            var currentUser = getCurrentUser();
            ViewBag.Message = "Welcome Customer Service :" + currentUser.FirstName + " " + currentUser.LastName + "!";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}