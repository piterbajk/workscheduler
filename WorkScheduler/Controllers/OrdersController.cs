﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkScheduler.Models;
using Microsoft.AspNet.Identity;

namespace WorkScheduler.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult BuildServicesTable()
        {
            var user = User.Identity.GetUserId();
            return View(db.Services.Where(x => x.OrderId == 0).ToList());
        }

        public ActionResult servicesInOrderPartialView()
        {
            return View("Index", "Services");
        }

        // GET: Orders
        public async Task<ActionResult> Index()
        {
            return View(await db.Orders.OrderBy(x => x.PlacementDate).ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = await db.Orders.FindAsync(id);
            ViewBag.Customers = GetCustomerList();

            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        private int getNextOrderId()
        {
            var nextOrderId = db.Orders
                .OrderByDescending(o => o.OrderId)
                .Select(o => o.OrderId)
                .FirstOrDefault();
            return nextOrderId + 1;
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.Customers = GetCustomerList();
            ViewBag.ServiceList = db.Services.OrderByDescending(x => x.StartTime);

            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrderId,Description,Value,PaymentMethod,PlacementDate,IsCompleted,CustomerId")] Order order)
        {
            ModelState.Clear();
            if (ModelState.IsValid)
            {
                order.PlacementDate = DateTime.Now;
                order.IsCompleted = false;
                order.Value = 0;
                Customer customer = db.Customers.FirstOrDefault
                    (x => x.CustomerId == order.CustomerId);
                order.Customer = customer;

                db.Orders.Add(order);
                await db.SaveChangesAsync();

                var orderId = order.OrderId;

                return RedirectToAction("Index");
            }
            ViewBag.Customers = GetCustomerList();
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            order.CustomerId = id;

            if (db.Services.Where(s => s.OrderId == order.OrderId).Count() != 0)
            {
                order.Value = db.Services.Where(s => s.OrderId == order.OrderId).Sum(s => s.Cost);
            }
            else
            {
                order.Value = 0;
            }

            ViewBag.Customers = GetCustomerList();
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrderId,Description,Value,PaymentMethod,PlacementDate,IsCompleted,CustomerId")] Order order)
        {
            if (ModelState.IsValid)
            {
                order.Value = db.Services.Where(s => s.OrderId == order.OrderId).Sum(s => s.Cost);
                db.Entry(order).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Customers = GetCustomerList();

            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            db.Orders.Remove(order);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetCustomers()
        {
            List<Customer> customers = new List<Customer>();
            customers = db.Customers.OrderBy(x => x.CompanyName).ToList();
            return Json( customers, JsonRequestBehavior.AllowGet );
        }

        public List<Customer> GetCustomerList()
        {
            var customers = db.Customers.OrderBy(x => x.CompanyName);
            return customers.ToList();
        }
    }
}
